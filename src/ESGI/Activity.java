package ESGI;

/**
 * Created by Vincent on 23/01/2017.
 */
public class Activity {
    String type;
    int begin;
    int end;

    public Activity(String type, int begin, int end) {
        this.type = type;
        this.begin = begin;
        this.end = end;
    }

    @Override
    public String toString() {
        return type + " : from " + begin + " to " + end;
    }
}
