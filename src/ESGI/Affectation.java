package ESGI;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vincent on 03/01/2017.
 */
public class Affectation {
    Worker worker;
    String date;
    String task;

    public Affectation(Worker worker, String date, String task) {
        this.worker = worker;
        this.date = date;
        this.task = task;
    }
}
