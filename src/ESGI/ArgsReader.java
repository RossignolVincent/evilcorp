package ESGI;

import com.google.gson.*;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Method;


/**
 * Created by Vincent on 24/01/2017.
 */
public class ArgsReader {
    Factory factory;
    String[] args;
    String filename;

    public ArgsReader(String[] args, Factory factory) throws FileNotFoundException {

        this.factory = factory;
        this.args = args;

        if(args[0].equals("--file")){
            this.filename = args[1];
            fileRead();
        } else {
            commandLineRead();
        }
    }

    public Factory getFactory() {
        return factory;
    }

    public void commandLineRead(){
        String task = this.args[1];
        String name = this.args[2];
        String date = this.args[6]==null?"today":this.args[6];
        String a = this.args[3];
        int begin = Integer.parseInt(this.args[4]);
        int end = Integer.parseInt(this.args[5]);

        Worker w;

        if(this.args[0].equals("activity")){
            w = new HumanWorker(name);
        } else if (this.args[0].equals("robot")){
            w = new RobotWorker(name);
        } else {
            return;
        }

        this.factory.affect(w, date, task);
        addActivity(w, a, begin, end);
    }

    public void fileRead() throws FileNotFoundException {
        JsonElement jelement = new JsonParser().parse(new FileReader(this.filename));
        JsonObject  jobject = jelement.getAsJsonObject();
        JsonArray jarrayRoles = jobject.getAsJsonArray("roles");
        JsonArray jarrayActivities = jobject.getAsJsonArray("activities");
        JsonArray jarrayRobots = jobject.getAsJsonArray("robots");

        for(JsonElement je : jarrayRoles){
            Worker w = null;
            String name = je.getAsJsonObject().get("name").toString();

            for(JsonElement robots : jarrayRobots){
                if(robots.getAsJsonObject().get("name").toString().equals(name)){
                    w = new RobotWorker(name);
                }

            }
            for(JsonElement humans : jarrayActivities){
                if(humans.getAsJsonObject().get("name").toString().equals(name))
                    w = new HumanWorker(name);
            }

            if(w == null)
                return;

            this.factory.affect(w, je.getAsJsonObject().get("start").toString(), je.getAsJsonObject().get("role").toString());

            if(w.getClass() == RobotWorker.class){
                for(JsonElement robots : jarrayRobots){
                    if(robots.getAsJsonObject().get("name").toString().equals(name)){
                        addActivity(w, robots.getAsJsonObject().get("activity").toString().replace("\"", ""), Integer.parseInt(robots.getAsJsonObject().get("start").toString()), Integer.parseInt(robots.getAsJsonObject().get("finish").toString()));
                    }
                }
            } else if(w.getClass() == HumanWorker.class){
                for(JsonElement humans : jarrayActivities){
                    if(humans.getAsJsonObject().get("name").toString().equals(name)){
                        addActivity(w, humans.getAsJsonObject().get("activity").toString().replace("\"", ""), Integer.parseInt(humans.getAsJsonObject().get("start").toString()), Integer.parseInt(humans.getAsJsonObject().get("finish").toString()));
                    }
                }
            } else {
                return;
            }
        }
    }

    public void addActivity(Worker w, String a, int begin, int end){
        a = a.toLowerCase();
        Class c = w.getClass();
        Method[] methods = c.getMethods();
        for(Method m : methods){
            if(m.getName().equals(a)){
                w.addActivity(a, begin, end);
            }
        }
    }
}
