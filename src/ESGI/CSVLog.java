package ESGI;

/**
 * Created by Vincent on 23/01/2017.
 */
public class CSVLog {

    public Factory factory;

    public CSVLog(Factory factory){
        this.factory = factory;
    }

    public void writeCSVLog(){
        for(Affectation affectation : factory.list) {
            for(Activity activity : affectation.worker.activities){
                System.out.println(affectation.task+";"+affectation.date+";"+affectation.worker.name+";"+activity.type+";"+activity.begin+";"+activity.end);
            }
        }
    }
}
