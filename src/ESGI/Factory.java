package ESGI;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vincent on 03/01/2017.
 */
public class Factory {

    public List<Affectation> list;

    public Factory () {
        list = new ArrayList<Affectation>();
    }

    public void affect(Worker worker, String date, String task){
        for(Affectation a : list){
            if(worker.name.equals(a.worker.name)){
                a.date = date;
                a.task = task;
                return;
            }
        }
        list.add(new Affectation(worker, date, task));
    }

    public void task1(Worker worker, String date) {
        affect(worker, date, "task1");
    }

    public void task2(Worker worker, String date) {
        affect(worker, date, "task2") ;
    }

    public void task3(Worker worker, String date){
        affect(worker, date, "task3");
    }
}
