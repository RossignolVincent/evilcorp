package ESGI;

/**
 * Created by Vincent on 23/01/2017.
 */
public class HumanWorker extends Worker {

    public HumanWorker(String name){
        super(name);
    }

    @Override
    public void work(int begin, int end) {
        addActivity("work", begin, end);
    }

    public void eat(int begin, int end) {
        addActivity("eat", begin, end);
    }

    public void sleep(int begin, int end) {
        addActivity("sleep", begin, end);
    }

    public void other(int begin, int end) {
        addActivity("other", begin, end);
    }

    @Override
    public void addActivity(String type, int begin, int end){
        activities.add(new Activity(type, begin, end));
    }
}
