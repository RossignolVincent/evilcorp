package ESGI;

/*
https://gist.github.com/rhwy/dddf59092302df5b4a54951ab61576ca
https://gist.github.com/rhwy/077d216484487672fd3c029af4a601a2
 */

import java.io.FileNotFoundException;

public class Main {

    public static void main(String[] args) {
	    Factory factory = new Factory();

        try {
            factory = new ArgsReader(args, factory).getFactory();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        /*
        HumanWorker Charles = new HumanWorker("Charles");
        factory.affect(Charles, "01/12/2017", "task1");
        Charles.work(8, 20);
        Charles.sleep(20, 8);

        RobotWorker BB8 = new RobotWorker("BB8");
        factory.affect(BB8, "01/12/2017", "task2");
        BB8.work(8, 20);
        BB8.standby(20, 8);*/

        CSVLog csvLog = new CSVLog(factory);
        csvLog.writeCSVLog();

    }
}
