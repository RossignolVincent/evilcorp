package ESGI;

/**
 * Created by Vincent on 23/01/2017.
 */
public class RobotWorker extends Worker {

    public RobotWorker(String name){
        super(name);
    }

    @Override
    public void work(int begin, int end) {
        addActivity("work", begin, end);
    }

    public void standby(int begin, int end) {
        addActivity("standby", begin, end);
    }

    @Override
    public void addActivity(String type, int begin, int end){
        activities.add(new Activity(type, begin, end));
    }
}
