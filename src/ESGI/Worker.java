package ESGI;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vincent on 03/01/2017.
 */
public abstract class Worker {

    public String name;
    public List<Activity> activities;

    public Worker(String name) {
        this.name = name;
        activities = new ArrayList<Activity>();
    }

    public abstract void work(int begin, int end);

    public abstract void addActivity(String type, int begin, int end);
}
